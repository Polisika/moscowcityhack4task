from datetime import date

import pandas as pd
import matplotlib.pyplot as plt


def main():
    df = pd.read_csv(
        "../data/transactions.csv",
        parse_dates=[
            "create_date",
        ],
    )

    # types
    df["create_date"] = pd.to_datetime(df["create_date"]).dt.date
    df["birth_year"] = df["birth_date"].fillna(-9999).astype(int)
    df["start_year"] = df["start_date"].fillna(-9999).astype(int)
    df["fact_close_year"] = df["fact_close_date"].fillna(-9999).astype(int)

    df["businessman_flag"] = df["businessman_flag"].astype(int)

    df.drop(["start_date", "birth_date", "fact_close_date"], inplace=True, axis=1)

    df["age"] = date.today().year - df["birth_year"]

    df.drop(["term", "businessman_flag"], axis=1, inplace=True)

    cliend_df = df[
        [
            "client_id",
            "gender",
            "birth_year",
            "city",
            "create_date",
            "nonresident_flag",
        ]
    ].drop_duplicates()

    card_df = df[
        [
            "card_id",
            "product_category_name",
            "card_type_name",
            "card_type",
            "start_year",
            "fact_close_year",
        ]
    ].drop_duplicates()

    client_x_cards_df = df[["client_id", "card_id"]].drop_duplicates()
    time_aggregations_df = df[
        [
            # index
            "client_id",
            "card_id",
            "purchase_count",
            "purchase_sum",
            "current_balance_avg_sum",
            "current_balance_sum",
            "current_debit_turn_sum",
            "current_credit_turn_sum",
        ]
    ]  # .drop_duplicates()

    time_aggregations_df["rn"] = (
        time_aggregations_df.groupby(["client_id", "card_id"]).cumcount() + 1
    )
    time_aggregations_df.shape


if __name__ == "__main__":
    main()
