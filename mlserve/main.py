import asyncio
import os
from cmath import e

import catboost
import fastapi
import httpx
import pandas as pd
import psycopg2
import uvicorn
from fastapi_utils.tasks import repeat_every
from sqlalchemy import create_engine

from utils import Features, VKInfo, get_analytics_info, predict_rules

app = fastapi.FastAPI()
model = catboost.CatBoostClassifier().load_model("model.cbm")


@app.post("/predict")
async def predict(r_ml: list[Features]):
    """
    :return: список
    результат модели, для дебетовых, для ИП, для кредитных карт, для кредит наличными
    """
    result = format_predict(r_ml)
    return result


async def format_predict(r_ml):
    rules = await predict_rules(r_ml)
    r_ml = [(i.gender, i.resident, i.city, i.age) for i in r_ml]
    res = model.predict(r_ml).tolist()
    result = [(m, i[0], i[1], i[2], i[3]) for m, i in zip(res, rules)]
    return result


async def translate_cities(set_cities):
    async with httpx.AsyncClient(
        headers={
            "Authorization": f"Bearer {os.environ['IAM_TOKEN']}",
            "Content-Type": "application/json",
        }
    ) as client:
        result = await client.post(
            "https://translate.api.cloud.yandex.net/translate/v2/translate",
            json={
                "folderId": "b1g9upucjrsft2sm8q27",
                "texts": set_cities,
                "targetLanguageCode": "ru",
            },
        )

    translations = {}
    for k, v in zip(set_cities, result.json()["translations"]):
        if "text" not in v:
            translations[k] = None
            continue

        translations[k] = v["text"]

    return translations


@app.post("/vk_predict")
async def vk_predict(req: VKInfo):
    with psycopg2.connect(dsn=os.environ["POSTGRESQL_DSN"]) as conn:
        with conn.cursor() as cur:
            cur.execute(
                f"""
                SELECT u.user_id, u.sex, u.resident, u.city, u.age
                FROM vk_user u 
                INNER JOIN vk_group_members g
                ON g.user_id = u.user_id
                WHERE g.group_id = {req.group_id};
            """
            )
            columns = [column[0] for column in cur.description]
            users = []
            for row in cur.fetchall():
                users.append(dict(zip(columns, row)))

    users_id = [i["user_id"] for i in users]
    get_translate = await translate_cities(list(set([i["city"] for i in users])))

    features = []
    try:
        for i in users:
            feat = dict(
                gender=i["sex"],
                resident=i["resident"],
                city=get_translate.get(i["city"]),
                age=i["age"],
            )
            # print(feat)
            features += [Features(**feat)]
    except Exception as e:
        print(i)
        raise e

    pr = await format_predict(features)
    values = [
        (user_id, p[0], p[1], p[2], p[3], p[4]) for user_id, p in zip(users_id, pr)
    ]

    url = os.environ["POSTGRESQL_DSN"]
    engine = create_engine(url)
    con = engine.connect()
    predict_schema = [
        "user_id",
        "model_result",
        "debet_rule",
        "ip_rule",
        "credit_rule",
        "nal_rule",
    ]
    df = pd.DataFrame(values, columns=predict_schema)
    df.to_sql("predict_user", con=con, if_exists="append")

    res_d = {
        "group_id": req.group_id,
        "scored_users_num": len(features),
        "model_result": int(df["model_result"].sum()),
        "debet_rule": int(df["debet_rule"].sum()),
        "ip_rule": int(df["ip_rule"].sum()),
        "credit_rule": int(df["credit_rule"].sum()),
        "nal_rule": int(df["nal_rule"].sum()),
    }
    print(res_d)
    return res_d


@app.on_event("startup")
@repeat_every(seconds=60 * 30)
async def startup():
    await get_analytics_info()


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8080)
