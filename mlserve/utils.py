import json
import os
from datetime import datetime, timedelta
from typing import List, Optional

import pandas as pd
import psycopg2
from pydantic import BaseModel, validator
from pytrends.request import TrendReq
from sqlalchemy import create_engine

with open("cities.json", "r") as f:
    cities = set(json.load(f))


class Features(BaseModel):
    gender: Optional[int]
    resident: str
    city: Optional[str]
    age: Optional[int]

    @validator("gender")
    def val_g(cls, v):
        if v is None:
            return 1

        assert v in (0, 1)
        return v

    @validator("resident")
    def val_r(cls, v):
        assert v in ("R", "N")
        return v

    @validator("city")
    def val_city(cls, v):
        if v is None or v not in cities:
            v = "Неизвестен"
        return v

    @validator("age")
    def val_age(cls, v):
        if v is None:
            v = 25
        if v > 100:
            v = 16
        return v


class VKInfo(BaseModel):
    group_id: int


CITIES_BIGGER_1000 = [
    "Москва",
    "Санкт-Петербург",
    "Новосибирск",
    "Екатеринбург",
    "Казань",
    "Нижний Новгород",
    "Челябинск",
    "Самара",
    "Уфа",
    "Ростов-на-Дону",
    "Омск",
    "Красноярск",
    "Воронеж",
    "Пермь",
    "Волгоград",
]
CITIES_BIGGER_500 = [
    *CITIES_BIGGER_1000,
    "Краснодар",
    "Тюмень",
    "Саратов",
    "Тольятти",
    "Ижевск",
    "Барнаул",
    "Ульяновск",
    "Иркутск",
    "Хабаровск",
    "Махачкала",
    "Владивосток",
    "Ярославль",
    "Оренбург",
    "Томск",
    "Кемерово",
    "Новокузнецк",
    "Рязань",
    "Набережные Челны",
    "Киров",
    "Севастополь",
    "Астрахань",
    "Балашиха",
    "Пенза",
]


def predict_ip(i):
    return i.gender == 1 and i.age >= 35 and i.city in CITIES_BIGGER_500


def predict_deb(i):
    return 35 <= i.age <= 54 and i.city in CITIES_BIGGER_1000


def predict_cred(i):
    return 35 <= i.age <= 54 and i.city in CITIES_BIGGER_500


def predict_nal(i):
    return i.age >= 35 and i.city not in CITIES_BIGGER_1000


async def predict_rules(r_ml) -> List:
    result = []
    for i in r_ml:
        subres = (
            int(predict_deb(i)),
            int(predict_ip(i)),
            int(predict_cred(i)),
            int(predict_nal(i)),
        )
        result.append(subres)
    return result


async def get_analytics_info():
    filter_bank = ["ОТП-Банк", "МТС-Банк", "Почта-Банк", "Альфа-Банк", "ВТБ"]

    with psycopg2.connect(dsn=os.environ["POSTGRESQL_DSN"]) as conn:
        with conn.cursor() as cursor:
            print("Get top-10 banks")

            cursor.execute("SELECT name FROM top_10_client_banks")
            kw_list = [i[0] for i in cursor.fetchall() if i[0] not in filter_bank]
        assert len(kw_list) == 5
        print(f"Top-5 banks: {kw_list}")

    print("Request data from Google Trends")
    pytrends = TrendReq()
    pytrends.build_payload(kw_list)

    yt = datetime.now() - timedelta(days=1)
    now = datetime.now()
    every_hour = pytrends.get_historical_interest(
        kw_list,
        year_start=now.year,
        month_start=now.month,
        day_start=yt.day,
        hour_start=0,
        year_end=now.year,
        month_end=now.month,
        day_end=now.day,
        hour_end=now.hour,
        sleep=0,
    )
    queries = pytrends.related_queries()
    searches = pytrends.trending_searches(pn="russia")

    url = os.environ["POSTGRESQL_DSN"]
    engine = create_engine(url)
    con = engine.connect()
    res = []
    for bank_name, i in queries.items():
        i["top"]["bank_name"] = bank_name
        res.append(i["top"])

    print("Send data to PostgreSQL")
    pd.concat(res, axis=0).to_sql("keywords_banks", con=con, if_exists="replace")
    every_hour.to_sql("every_hour_keywords_gtrends", con=con, if_exists="replace")
    searches.to_sql("searches_keywords_gtrends", con=con, if_exists="replace")
