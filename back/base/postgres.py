from contextlib import contextmanager
from datetime import datetime

import logging
from sqlalchemy import BigInteger
from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import create_engine
from sqlalchemy import orm
from sqlalchemy.ext.declarative import as_declarative
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import Query
_pg_session = None


class PGSession:
    _cfg = None
    _session_factory = None
    Base = declarative_base()

    @classmethod
    def init_cfg(cls, cfg):
        if cls._cfg:
            raise Exception('PGSession already inited')
        cls._cfg = cfg
        if cls._session_factory is None:
            dsn = cls._cfg['postgresql']['dsn']
            engine = create_engine(dsn)
            cls._session_factory = orm.scoped_session(
                orm.sessionmaker(
                    autocommit=False,
                    autoflush=True,
                    bind=engine,
                ),
            )

    @classmethod
    @contextmanager
    def session(cls, expire_on_commit: bool = False):
        if cls._session_factory is None:
            raise Exception('session factory doesnot exist')
        session: orm.Session = cls._session_factory()
        session.expire_on_commit = expire_on_commit
        try:
            yield session
        except Exception:
            logging.exception('Session rollback because of exception')
            session.rollback()
            raise
        finally:
            session.close()


@as_declarative()
class PGBase:

    @classmethod
    def query(cls, *args) -> Query:
        """
        Метод, чтобы делать запросы от модели
        :param args: столбцы модели
        :return:
        """
        if not args:
            args = cls,

        with PGSession.session() as pg_session:
            result = pg_session.query(*args)
        return result

    def save(self):
        with PGSession.session() as pg_session:
            pg_session.add(self)
            pg_session.commit()
            pg_session.refresh(self)
        return self


def insert_batch(models: list[PGBase]):
    with PGSession.session() as pg_session:
        pg_session.bulk_save_objects(models)
        pg_session.commit()
        pg_session.refresh


class CreatedAtMixIn:

    created_at = Column(DateTime, default=datetime.now, index=True, nullable=False)


class DateTimeMixIn(CreatedAtMixIn):

    updated_at = Column(DateTime, default=datetime.now, onupdate=datetime.now, nullable=False)


class IdMixIn:

    id = Column(BigInteger, primary_key=True)
