from sqlalchemy import Boolean, Column, Integer, String

from base.postgres import PGBase, DateTimeMixIn


class AnalyzingStatModel(PGBase, DateTimeMixIn):
    __tablename__ = 'analyzing_stat'

    analyzing_req_id = Column(Integer, primary_key=True, autoincrement=True)
    analyzing_link = Column(String(256))
    error = Column(Boolean, default=None)
    evaluated_clients_amount = Column(Integer, default=None)
    advertising_price = Column(Integer, default=None)
    debet_rule = Column(Integer, default=None)
    ip_rule = Column(Integer, default=None)
    credit_rule = Column(Integer, default=None)
    cash_credit_rule = Column(Integer, default=None)
