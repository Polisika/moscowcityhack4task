from sqlalchemy import Column, Integer, String

from base.postgres import PGBase, DateTimeMixIn


class VkUserModel(PGBase, DateTimeMixIn):
    __tablename__ = 'vk_user'

    user_id = Column(Integer, primary_key=True)
    city = Column(String, default=None)
    age = Column(Integer, default=None)
    resident = Column(String, default='R')
    sex = Column(Integer, default=None)


class VkGroupModel(PGBase, DateTimeMixIn):
    __tablename__ = 'vk_group'

    group_id = Column(Integer, primary_key=True)
    group_name = Column(String)
    members_count = Column(Integer)


class VkGroupMembersModel(PGBase, DateTimeMixIn):
    __tablename__ = 'vk_group_members'

    group_id = Column(Integer, primary_key=True)
    user_id = Column(Integer, primary_key=True)
