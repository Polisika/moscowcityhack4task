import json
import os
from time import sleep
from typing import Union
from datetime import date, datetime

import requests
from dateutil.relativedelta import relativedelta

from base.pg_models.analysing_stat import AnalyzingStatModel
from base.pg_models.vk_analytics import VkGroupMembersModel, VkGroupModel, VkUserModel
from base.postgres import PGSession, insert_batch
from base.rabbitmq import RabbitConsumer, RabbitPublisher


class VkParserProcessor:
    _api_version = '5.131'

    def __init__(self, access_token: str, rabbit_cfg: dict):
        self._access_token = access_token
        self._api_url = 'https://api.vk.com/method'
        self._groups_domain = '/groups'
        self._users_domain = '/users'
        self._parsing_queue = RabbitConsumer(rabbit_cfg, 'parsing_queue')
        self._scoring_queue = RabbitPublisher(rabbit_cfg, 'scoring_queue')

    def run(self):
        for req_id, group_link in self._next_msg():
            group_id = group_link.split('/')[-1]
            print('parsing group info')
            group_info = self._get_group_info(group_id)
            print(group_id, group_info)
            if not group_info:
                print('No group data was fetched. Check group id')
                self._save_error(req_id)
                continue
            group_id = group_info[0]['id']
            check_model = VkGroupModel.query().filter(VkGroupModel.group_id == group_id).one_or_none()
            if not check_model:
                model = VkGroupModel(
                    group_id=group_id,
                    members_count=group_info[0]['members_count'],
                    group_name=group_info[0]['name'],
                )
                model.save()
            print('parsing group members')
            group_members = self._get_group_members(group_id)
            print(group_id, group_members)
            if not group_members:
                print('No group members were fetched. Check group id and permissions')
                self._save_error(req_id)
                continue
            check_model = VkGroupMembersModel.query(VkGroupMembersModel.user_id).filter(
                VkGroupMembersModel.group_id == group_id,
            )
            existent_member_ids = {member.user_id for member in check_model}
            models_to_add = []
            for member_id in group_members['items']:
                if member_id not in existent_member_ids:
                    models_to_add.append(VkGroupMembersModel(
                        group_id=group_id,
                        user_id=member_id
                    ))
            insert_batch(models_to_add)
            print('parsing users')
            users_info = self._get_user(group_members['items'])
            print(users_info)
            if not users_info:
                print('No user info was fetched. Check user id and permissions')
                self._save_error(req_id)
                continue
            check_model = VkUserModel.query(
                VkUserModel.user_id,
            ).filter(VkUserModel.user_id.in_([user['id'] for user in users_info]))
            existent_user_ids = {user.user_id for user in check_model}
            models_to_add = []
            for user_info in users_info:
                if user_info['id'] not in existent_user_ids:
                    age = relativedelta(
                        date.today(),
                        datetime.strptime(user_info.get('bdate'), '%d.%m.%Y').date(),
                    ).years if user_info.get('bdate') and len(user_info.get('bdate').split('.')) == 3 else None
                    models_to_add.append(VkUserModel(
                        user_id=user_info['id'],
                        city=user_info.get('city', {}).get('title', user_info.get('home_town')),
                        age=age,
                        sex=user_info.get('sex') - 1 if user_info.get('sex') else None,
                    ))
            insert_batch(models_to_add)
            print('finish parsing')
            self._scoring_queue.send(json.dumps({
                'group_id': group_id, 'analyzing_req_id': req_id, 'evaluated_clients_amount': 1000,
            }))

    def _next_msg(self) -> tuple[str, str]:
        while True:
            msg = self._parsing_queue.get_one_message()
            msg = json.loads(msg)
            yield msg['analyzing_req_id'], msg['group_link']
            self._parsing_queue.ack()

    def _save_error(self, req_id: str):
        model = AnalyzingStatModel.query().filter(
            AnalyzingStatModel.analyzing_req_id == req_id,
        ).one_or_none()
        model.error = True
        model.save()

    def _get_group_info(self, group_id: str):
        group_info = self._raw_request(
            self._groups_domain,
            'getById',
            {'group_id': group_id, 'fields': 'members_count'},
        )
        print('group_info', group_info)
        if group_info.get('error'):
            return []
        return group_info['response']

    def _get_group_members(self, group_id: str, count: int = 1000, offset: int = 0):
        group_members = self._raw_request(
            self._groups_domain,
            'getMembers',
            {'group_id': group_id, 'count': count, 'offset': offset}
        )
        if group_members.get('error'):
            return []
        print('group_members', group_members)
        return group_members['response']

    def _get_user(self, user_ids: list[str]):
        user_info = self._raw_request(
            self._users_domain,
            'get',
            {'user_ids': ', '.join(map(str, user_ids)), 'fields': 'city, bdate, sex,home_town'}
        )
        print('user_info', user_info)
        return user_info['response']

    def _get_users_subscriptions(self, user_id: str):
        users_subscriptions = self._raw_request(
            self._groups_domain,
            'get',
            {'user_id': user_id}
        )
        print('users_subscriptions', users_subscriptions)
        return users_subscriptions['response']

    def _raw_request(self, domain: str, method: str, params: dict) -> Union[dict, list]:
        params.update(access_token=self._access_token, v=self._api_version)
        response = requests.get(self._api_url + domain + '.' + method, params=params)
        return response.json()


if __name__ == '__main__':
    rabbit_cfg = {
        'host': os.getenv('RABBITMQ_HOST'),
        'port': os.getenv('RABBITMQ_PORT'),
        'user': os.getenv('RABBITMQ_DEFAULT_USER'),
        'password': os.getenv('RABBITMQ_DEFAULT_PASS'),
    }
    PGSession.init_cfg({
        'postgresql': {
            'dsn': os.getenv('POSTGRESQL_DSN')
        },
    })
    VkParserProcessor(os.getenv('VK_API_TOKEN'), rabbit_cfg).run()
