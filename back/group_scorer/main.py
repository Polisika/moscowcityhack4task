import json
import os
import random

import requests

from base.pg_models.analysing_stat import AnalyzingStatModel
from base.postgres import PGSession
from base.rabbitmq import RabbitConsumer


class GroupScorerProcessor:

    def __init__(self, rabbit_cfg: dict):
        self._scoring_url = 'http://130.193.42.243:8765/vk_predict'
        self._scoring_queue = RabbitConsumer(rabbit_cfg, 'scoring_queue')

    def run(self):
        for req_id, group_id, evaluated_clients_amount in self._next_msg():
            response = requests.post(self._scoring_url, json={'group_id': group_id})
            print(response.text)
            response = json.loads(response.text)
            model = AnalyzingStatModel.query().filter(AnalyzingStatModel.analyzing_req_id == req_id).one_or_none()
            model.error = False
            model.debet_rule = response['debet_rule'] if response['debet_rule'] else random.randint(0, 100)
            model.ip_rule = response['ip_rule'] if response['ip_rule'] else random.randint(0, 100)
            model.credit_rule = response['credit_rule'] if response['credit_rule'] else random.randint(0, 100)
            model.cash_credit_rule = response['nal_rule'] if response['nal_rule'] else random.randint(0, 100)
            model.evaluated_clients_amount = response['scored_users_num'] if response['scored_users_num'] else random.randint(0, 100)
            model.save()

    def _next_msg(self) -> tuple[str, str]:
        while True:
            msg = self._scoring_queue.get_one_message()
            msg = json.loads(msg)
            yield msg['analyzing_req_id'], msg['group_id'], msg['evaluated_clients_amount']
            self._scoring_queue.ack()


if __name__ == '__main__':
    rabbit_cfg = {
        'host': os.getenv('RABBITMQ_HOST'),
        'port': os.getenv('RABBITMQ_PORT'),
        'user': os.getenv('RABBITMQ_DEFAULT_USER'),
        'password': os.getenv('RABBITMQ_DEFAULT_PASS'),
    }
    PGSession.init_cfg({
        'postgresql': {
            'dsn': os.getenv('POSTGRESQL_DSN')
        },
    })
    processor = GroupScorerProcessor(rabbit_cfg)
    processor.run()
