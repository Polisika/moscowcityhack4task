import json
import os
from typing import Optional

import uvicorn
from fastapi import FastAPI
from pydantic import BaseModel
from starlette.responses import JSONResponse
from starlette.requests import Request

from base.pg_models.analysing_stat import AnalyzingStatModel
from base.postgres import PGSession
from base.rabbitmq import RabbitPublisher

app = FastAPI()


class AnalyzingResult(BaseModel):
    evaluated_clients_amount: Optional[int] = None
    advertising_price: Optional[int] = None
    debet_rule: Optional[int] = None
    ip_rule: Optional[int] = None
    credit_rule: Optional[int] = None
    cash_credit_rule: Optional[int] = None
    error: Optional[bool] = None


@app.post("/analyze/social/vk/group", response_class=JSONResponse)
async def analyze(request: Request, group_link: str):
    model = AnalyzingStatModel(analyzing_link=group_link).save()
    request.app.parsing_queue.send(json.dumps({'group_link': group_link, 'analyzing_req_id': model.analyzing_req_id}))
    return JSONResponse({'analyzing_req_id': model.analyzing_req_id})


@app.get("/analyze/social/vk/group", response_model=AnalyzingResult)
async def get_result(analyzing_req_id: int):
    model = AnalyzingStatModel.query().filter(
        AnalyzingStatModel.analyzing_req_id == analyzing_req_id
    ).one_or_none()
    if model:
        return AnalyzingResult(
            evaluated_clients_amount=model.evaluated_clients_amount,
            advertising_price=model.advertising_price,
            debet_rule=model.debet_rule,
            ip_rule=model.ip_rule,
            credit_rule=model.credit_rule,
            cash_credit_rule=model.cash_credit_rule,
            error=model.error,
        )
    return AnalyzingResult()


def init_app():
    rabbit_cfg = {
        'host': os.getenv('RABBITMQ_HOST'),
        'port': os.getenv('RABBITMQ_PORT'),
        'user': os.getenv('RABBITMQ_DEFAULT_USER'),
        'password': os.getenv('RABBITMQ_DEFAULT_PASS'),
    }
    PGSession.init_cfg({
        'postgresql': {
            'dsn': os.getenv('POSTGRESQL_DSN')
        },
    })
    app.parsing_queue = RabbitPublisher(rabbit_cfg, 'parsing_queue')


init_app()


if __name__ == '__main__':
    uvicorn.run(app, host='0.0.0.0', port=8080)
