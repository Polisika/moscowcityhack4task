import streamlit as st
from consts import (sidebar_classic,
                    sidebar1_VK, sidebar1_sites,
                    sidebar1_Ozon, VK, sidebar1_Wildberries)


#                     sidebar2_mark_place, sidebar2_object,
#                     sidebar2_competitor_advertising_analysis, sidebar2_changing_audience,
#                     sidebar2_marginality)
# from PIL import Image

# with st.sidebar:
#     image = Image.open('front/images/label.jpeg')
#     st.image(image, width=100)


def sidebar1():
    # Using "with" notation
    with st.sidebar:
        var1 = st.selectbox(
            "Функции сервиса",
            (sidebar_classic,
             sidebar1_VK,
             sidebar1_sites,
             sidebar1_Ozon,
             VK,
             sidebar1_Wildberries
             )
        )
        return var1
