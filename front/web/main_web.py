import streamlit as st
from sidebar import sidebar1
from utils import (filter_competitors, marginality, competitor_advertising_analysis,
                   changing_audience, object_competitors)
import pandas as pd
from PIL import Image
import numpy as np
import datetime
import plotly.figure_factory as ff
import scipy
import plotly.express as px
import random
import httpx
import time

# def graph_from_grafana(link: str) -> None:
#     url = f'''<iframe src="{link}" width="700" height="1500" frameborder="0"></iframe>
#     '''
#     st.markdown(url, unsafe_allow_html=True)


st.set_page_config(
    page_title="Earn money developers",
    page_icon="💰",
    layout="centered",
    # initial_sidebar_state='expanded'
)

with st.sidebar:
    image = Image.open('images/label.jpeg')
    st.image(image)
    st.markdown("")


def _max_width_():
    max_width_str = f"max-width: 1400px;"
    st.markdown(
        f"""
    <style>
    .reportview-container .main .block-container{{
        {max_width_str}
    }}
    </style>
    """,
        unsafe_allow_html=True,
    )


_max_width_()

match resp := sidebar1():
    case "⚙ Выберите вариант":

        st.title('📈 Рекомендательный сервис для продвижения продуктов банка')

        with st.expander("ℹ️ - О нашем сервисе", expanded=True):
            st.write(
                """
        🏦  Web-сервис, который поможет сотрудникам банка оценивать эффективность цифровых каналов продвижения
            банковских продуктов на основе данных о целевой аудитории банка и активности конкурентов.
                """
            )
    case "‍👤🏦 Аналитика клиентов":
        st.title("‍👤🏦 Аналитика клиентов")
        st.markdown('<iframe src="http://130.193.42.243:3000/d-solo/iMIXScj7z/segment-analytics'
                    '?orgId=1&from=1591920000000&to=1652313600000&var-main_bank=rshb.ru&panelId=11"'
                    ' width="750" height="410" '
                    'frameborder="0"></iframe>',
                    unsafe_allow_html=True)

        st.markdown('<iframe src="http://130.193.42.243:3000/d-solo/iMIXScj7z/segment-analytics?orgId=1&from=1591920000'
                    '000&to=1652313600000&var-main_bank=rshb.ru&panelId=13"'
                    ' width="750" height="410" '
                    'frameborder="0"></iframe>',
                    unsafe_allow_html=True)

        st.markdown('<iframe src="http://130.193.42.243:3000/d-solo/iMIXScj7z/segment-analytics?orgId=1&from=159192000'
                    '0000&to=1652313600000&var-main_bank=rshb.ru&panelId=31"'
                    ' width="750" height="410" '
                    'frameborder="0"></iframe>',
                    unsafe_allow_html=True)

        st.markdown('<iframe src="http://130.193.42.243:3000/d-solo/iMIXScj7z/segment-analytics?orgId=1&from=159192000'
                    '0000&to=1652313600000&var-main_bank=rshb.ru&panelId=32"'
                    ' width="750" height="410" '
                    'frameborder="0"></iframe>',
                    unsafe_allow_html=True)
    case "🖥 Поисковые запросы":
        st.title("🖥 Поисковые запросы")
        st.markdown('<iframe src="http://130.193.42.243:3000/d-solo/iMIXScj7z/segment-analytics?orgId=1&from=15919200'
                    '00000&to=1652313600000&var-main_bank=rshb.ru&panelId=15"'
                    ' width="750" height="410" '
                    'frameborder="0"></iframe>',
                    unsafe_allow_html=True)

        # st.markdown('<iframe src="http://130.193.42.243:3000/d-solo/iMIXScj7z/segment-analytics?orgId=1&from=16549920'
        #             '00000&to=1655053200000&var-main_bank=rshb.ru&var-gtrends_bank_name=%D0%A1%D0%B1%D0%B5%D1%80%D0%B1'
        #             '%D0%B0%D0%BD%D0%BA&panelId=34"'
        #             ' width="750" height="410" '
        #             'frameborder="0"></iframe>',
        #             unsafe_allow_html=True)

        st.markdown('<iframe src="http://130.193.42.243:3000/d-solo/iMIXScj7z/segment-analytics?orgId=1&from=1591920'
                    '000000&to=1652313600000&var-main_bank=rshb.ru&panelId=29"'
                    ' width="750" height="410" '
                    'frameborder="0"></iframe>',
                    unsafe_allow_html=True)

        st.markdown('<iframe src="http://130.193.42.243:3000/d-solo/iMIXScj7z/segment-analytics?orgId=1&from=1591920000'
                    '000&to=1652313600000&var-main_bank=rshb.ru&var-gtrends_bank_name=%D0%A1%D0%B1%D0%B5%D1%80%D0%B1%D'
                    '0%B0%D0%BD%D0%BA&panelId=34"'
                    ' width="750" height="410" '
                    'frameborder="0"></iframe>',
                    unsafe_allow_html=True)

        st.markdown('<iframe src="http://130.193.42.243:3000/d-solo/iMIXScj7z/segment-analytics?orgId=1&var-main_bank=s'
                    'ovcombank.ru&from=1655016605584&to=1655059805584&var-allsocial_category=%D0%B1%D0%B8%D0%B7%D0%BD%'
                    'D0%B5%D1%81&var-allsocial_source=VK&var-gtrends_bank_name=%D0%A2%D0%B8%D0%BD%D1%8C%D0%BA%D0%BE%D1'
                    '%84%D1%84&panelId=36"'
                    ' width="750" height="410" '
                    'frameborder="0"></iframe>',
                    unsafe_allow_html=True)

        bank = st.selectbox(
            '✅ Выберите банк, ключевые слова которого желаете посмотреть',
            ('Сбербанк', 'Совкомбанк', 'Открытие', 'Тинькофф', 'Газпромбанк'))
        match bank:
            case 'Сбербанк':
                st.markdown("""
                    <iframe src="http://130.193.42.243:3000/d-solo/FMEIATCnk/kliuchevye-slova-konkurentov?orgId=1&fro
                    m=1655043774795&to=1655065374795&panelId=2" width="750" height="410" frameborder="0"></iframe>
                    """, unsafe_allow_html=True)
            case 'Совкомбанк':
                st.markdown(
                    '<iframe src="http://130.193.42.243:3000/d-solo/FMEIATCnk/kliuchevye-slova-konkurentov?orgId=1&var'
                    '-gtrends_bank_name=%D0%A1%D0%BE%D0%B2%D0%BA%D0%BE%D0%BC%D0%B1%D0%B0%D0%BD%D0%BA&from=1655043880844'
                    '&to=1655065480844&panelId=2"'
                    ' width="750" height="410" '
                    'frameborder="0"></iframe>',
                    unsafe_allow_html=True)
            case 'Открытие':
                st.markdown(
                    '<iframe src="http://130.193.42.243:3000/d-solo/FMEIATCnk/kliuchevye-slova-konkurentov?orgId=1&va'
                    'r-gtrends_bank_name=%D0%9E%D1%82%D0%BA%D1%80%D1%8B%D1%82%D0%B8%D0%B5&from=1655046127921&to=165506'
                    '7727921&panelId=2"'
                    ' width="750" height="410" '
                    'frameborder="0"></iframe>',
                    unsafe_allow_html=True)
            case 'Тинькофф':
                st.markdown(
                    '<iframe src="http://130.193.42.243:3000/d-solo/FMEIATCnk/kliuchevye-slova-konkurentov?orgId=1&va'
                    'r-gtrends_bank_name=%D0%A2%D0%B8%D0%BD%D1%8C%D0%BA%D0%BE%D1%84%D1%84&from=1655046160993&to=1655'
                    '067760993&panelId=2"'
                    ' width="750" height="410" '
                    'frameborder="0"></iframe>',
                    unsafe_allow_html=True)
            case 'Газпромбанк':
                st.markdown(
                    '<iframe src="http://130.193.42.243:3000/d-solo/FMEIATCnk/kliuchevye-slova-konkurentov?orgId=1&v'
                    'ar-gtrends_bank_name=%D0%93%D0%B0%D0%B7%D0%BF%D1%80%D0%BE%D0%BC%D0%B1%D0%B0%D0%BD%D0%BA&from=165'
                    '5046183475&to=1655067783476&panelId=2"'
                    ' width="750" height="410" '
                    'frameborder="0"></iframe>',
                    unsafe_allow_html=True)

    case "💳 Использование продуктов банка":
        st.title("💳 Использование продуктов банка")
        st.markdown('<iframe src="http://130.193.42.243:3000/d-solo/iMIXScj7z/segment-analytics?orgId=1&from=1591'
                    '920000000&to=1652313600000&var-main_bank=rshb.ru&panelId=4"'
                    ' width="750" height="410" '
                    'frameborder="0"></iframe>',
                    unsafe_allow_html=True)

        st.markdown('<iframe src="http://130.193.42.243:3000/d-solo/iMIXScj7z/segment-analytics?orgId=1&from=159192000'
                    '0000&to=1652313600000&var-main_bank=rshb.ru&panelId=6"'
                    ' width="750" height="410" '
                    'frameborder="0"></iframe>',
                    unsafe_allow_html=True)

        st.markdown('<iframe src="http://130.193.42.243:3000/d-solo/iMIXScj7z/segment-analytics?orgId=1&from=1591920'
                    '000000&to=1652313600000&var-main_bank=rshb.ru&panelId=9"'
                    ' width="750" height="410" '
                    'frameborder="0"></iframe>',
                    unsafe_allow_html=True)

        st.markdown('<iframe src="http://130.193.42.243:3000/d-solo/iMIXScj7z/segment-analytics?orgId=1&from=15919200'
                    '00000&to=1652313600000&var-main_bank=rshb.ru&panelId=7"'
                    ' width="750" height="410" '
                    'frameborder="0"></iframe>',
                    unsafe_allow_html=True)

    case '💙 VK':
        image = Image.open('images/vk.jpeg')
        st.image(image, width=90)

        theme = st.selectbox(
            '✅ Выберите тему, на которую желаете посмотреть топ-20 популярных групп',
            ('Автомобили', 'Компании', 'Стартапы', 'Ремонт', 'Недвижимость', 'Путешествия',
             'Финансы', 'Магазины', 'Природа'))
        match theme:
            case 'Автомобили':
                st.markdown("""
                    <iframe src="http://130.193.42.243:3000/d-solo/8xSJDTC7z/populiarnye-istochniki?orgId=1&var
                    -allsocial_source=VK&var-allsocial_category=%D0%B0%D0%B2%D1%82%D0%BE%D0%BC%D0%BE%D0%B1%D0%B
                    8%D0%BB%D0%B8&from=1655048181219&to=1655069781219&panelId=2" width="750" height="410" 
                    frameborder="0"></iframe>""",
                            unsafe_allow_html=True)

            case 'Компании':
                st.markdown(
                    '<iframe src="http://130.193.42.243:3000/d-solo/8xSJDTC7z/populiarnye-istochniki?orgId=1&var-allsoc'
                    'ial_source=VK&var-allsocial_category=%D0%BA%D0%BE%D0%BC%D0%BF%D0%B0%D0%BD%D0%B8%D0%B8&from=165504'
                    '8264613&to=1655069864614&panelId=2"'
                    ' width="750" height="410" '
                    'frameborder="0"></iframe>',
                    unsafe_allow_html=True)
            case 'Стартапы':
                st.markdown(
                    '<iframe src="http://130.193.42.243:3000/d-solo/8xSJDTC7z/populiarnye-istochniki?orgId=1&var-allso'
                    'cial_source=VK&var-allsocial_category=%D1%81%D1%82%D0%B0%D1%80%D1%82%D0%B0%D0%BF%D1%8B&from=16550'
                    '48293915&to=1655069893915&panelId=2"'
                    ' width="750" height="410" '
                    'frameborder="0"></iframe>',
                    unsafe_allow_html=True)
            case 'Ремонт':
                st.markdown(
                    '<iframe src="http://130.193.42.243:3000/d-solo/8xSJDTC7z/populiarnye-istochniki?orgId=1&var-alls'
                    'ocial_source=VK&var-allsocial_category=%D1%80%D0%B5%D0%BC%D0%BE%D0%BD%D1%82&from=1655048323464&to='
                    '1655069923464&panelId=2"'
                    ' width="750" height="410" '
                    'frameborder="0"></iframe>',
                    unsafe_allow_html=True)
            case 'Недвижимость':
                st.markdown(
                    '<iframe src="http://130.193.42.243:3000/d-solo/8xSJDTC7z/populiarnye-istochniki?orgId=1&var-all'
                    'social_source=VK&var-allsocial_category=%D0%BD%D0%B5%D0%B4%D0%B2%D0%B8%D0%B6%D0%B8%D0%BC%D0%BE%D'
                    '1%81%D1%82%D1%8C&from=1655048358150&to=1655069958150&panelId=2"'
                    ' width="750" height="410" '
                    'frameborder="0"></iframe>',
                    unsafe_allow_html=True)

            case 'Путешествия':
                st.markdown(
                    '<iframe src="http://130.193.42.243:3000/d-solo/8xSJDTC7z/populiarnye-istochniki?orgId=1&var'
                    '-allsocial_source=VK&var-allsocial_category=%D0%BF%D1%83%D1%82%D0%B5%D1%88%D0%B5%D1%81%D1%82'
                    '%D0%B2%D0%B8%D1%8F&from=1655048387397&to=1655069987397&panelId=2"'
                    ' width="750" height="410" '
                    'frameborder="0"></iframe>',
                    unsafe_allow_html=True)

            case 'Финансы':
                st.markdown(
                    '<iframe src="http://130.193.42.243:3000/d-solo/8xSJDTC7z/populiarnye-istochniki?orgId=1&var-allsoc'
                    'ial_source=VK&var-allsocial_category=%D1%84%D0%B8%D0%BD%D0%B0%D0%BD%D1%81%D1%8B&from=165504843658'
                    '2&to=1655070036582&panelId=2"'
                    ' width="750" height="410" '
                    'frameborder="0"></iframe>',
                    unsafe_allow_html=True)

            case 'Магазины':
                st.markdown(
                    '<iframe src="http://130.193.42.243:3000/d-solo/8xSJDTC7z/populiarnye-istochniki?orgId=1&var-allso'
                    'cial_source=VK&var-allsocial_category=%D0%BC%D0%B0%D0%B3%D0%B0%D0%B7%D0%B8%D0%BD%D1%8B&from=16550'
                    '48461280&to=1655070061280&panelId=2"'
                    ' width="750" height="410" '
                    'frameborder="0"></iframe>',
                    unsafe_allow_html=True)

            case 'Природа':
                st.markdown(
                    '<iframe src="http://130.193.42.243:3000/d-solo/8xSJDTC7z/populiarnye-istochniki?orgId=1&var-alls'
                    'ocial_source=VK&var-allsocial_category=%D0%BF%D1%80%D0%B8%D1%80%D0%BE%D0%B4%D0%B0&from=165504848'
                    '3747&to=1655070083747&panelId=2"'
                    ' width="750" height="410" '
                    'frameborder="0"></iframe>',
                    unsafe_allow_html=True)

        # http://130.193.42.243:3000/d/Wmqco0C7z/analiz-grupp-vk?orgId=1&var-scored_group_names=%D0%A1%D0%B1%D0%B5%D1%80&var-scored_group_names=%D0%A2%D0%B8%D0%BD%D1%8C%D0%BA%D0%BE%D1%84%D1%84&from=1655075296465&to=1655096896465&viewPanel=6

        st.markdown(
            '<iframe src="http://130.193.42.243:3000/d/Wmqco0C7z/analiz-grupp-vk?orgId=1&var-scored_group_names=%D0%A'
            '1%D0%B1%D0%B5%D1%80&var-scored_group_names=%D0%A2%D0%B8%D0%BD%D1%8C%D0%BA%D0%BE%D1%84%D1%84&from=1655077'
            '401447&to=1655099001447"'
            ' width="750" height="750" '
            'frameborder="0"></iframe>',
            unsafe_allow_html=True)

        st.markdown("")
        st.markdown("")
        st.markdown("")
        exclude = st.text_input('🗒 Введите ссылку на группу, которую желаете проанализировать.'
                                ' ⏳ Занимает в среднем 30 секунд', value='https://vk.com/sber')
        # st.write()
        exclude = exclude.strip()
        ls = ('Ссылка на группу',
              'Оцененное кол-во клиентов',
              'Цена рекламы в группе',
              'Дебетовые карты',
              'Расчётный счёта для ИП',
              'Кредитные карты',
              'Кредит наличными')
        if exclude:
            response = httpx.post(url=f'http://130.193.42.243:8766/analyze/social/vk/group?group_link={exclude}')
            # st.write(response)
            if response.status_code == 200:
                response = response.json()
                # st.write(response)
                response = response['analyzing_req_id']
                # st.write(response)
                my_bar = st.progress(0)
                for percent_complete in range(100):
                    time.sleep(0.1)
                    my_bar.progress(percent_complete + 1)
                else:
                    st.balloons()
                status = httpx.get(url=f'http://130.193.42.243:8766/analyze/social/vk/group?analyzing_req_id='
                                       f'{int(response)}')
                status = status.json()
                if status['error'] is False:
                    df = pd.DataFrame(
                        [[exclude, status['evaluated_clients_amount'],
                          status['advertising_price'],
                          status['debet_rule'],
                          status['ip_rule'],
                          status['credit_rule'],
                          status['cash_credit_rule'],
                          ]
                         ],
                        columns=ls)
                    # st.markdown(('col %d' % i for i in range(5)))
                    st.table(df)
                else:
                    st.write('Извините, но сервис временно не доступен')
            else:
                st.write('Извините, но сервис временно не доступен')

    case '💎 Telegram':
        image = Image.open('images/tg.jpeg')
        st.image(image, width=90)

        theme = st.selectbox(
            '✅ Выберите банк, ключевые слова которого желаете посмотреть',
            ('Бизнес', 'Путешествия'))
        match theme:
            case 'Бизнес':
                st.markdown("""
                            <iframe src="http://130.193.42.243:3000/d-solo/8xSJDTC7z/populiarnye-istochniki?orgId=1&va
                            r-allsocial_source=TG&var-allsocial_category=%D0%B1%D0%B8%D0%B7%D0%BD%D0%B5%D1%81&from=165
                            5070267160&to=1655091867161&panelId=2
                            " width="750" height="410" frameborder="0"></iframe>
                            """, unsafe_allow_html=True)
            case 'Путешествия':
                st.markdown(
                    '<iframe src="http://130.193.42.243:3000/d-solo/8xSJDTC7z/populiarnye-istochniki?orgId=1&var-allsoc'
                    'ial_source=TG&var-allsocial_category=%D0%BF%D1%83%D1%82%D0%B5%D1%88%D0%B5%D1%81%D1%82%D0%B2%D0%B8'
                    '%D1%8F&from=1655070302623&to=1655091902623&panelId=2"'
                    ' width="750" height="410" '
                    'frameborder="0"></iframe>',
                    unsafe_allow_html=True)
# http://130.193.42.243:3000/d-solo/8xSJDTC7z/populiarnye-istochniki?orgId=1&var-allsocial_source=TG&var-allsocial_category=%D0%B1%D0%B8%D0%B7%D0%BD%D0%B5%D1%81&from=1655070267160&to=1655091867161&panelId=2
# http://130.193.42.243:3000/d-solo/8xSJDTC7z/populiarnye-istochniki?orgId=1&var-allsocial_source=TG&var-allsocial_category=%D0%BF%D1%83%D1%82%D0%B5%D1%88%D0%B5%D1%81%D1%82%D0%B2%D0%B8%D1%8F&from=1655070302623&to=1655091902623&panelId=2
#######################################################################################################################
#######################################################################################################################
# case 'VK':
#     match resp2 := sidebar2():
#         case 'Оценка площадки':
#             image = Image.open('images/vk.jpeg')
#             st.image(image, width=90)
#             # Group data together
#             hist_data = [[random.randint(0, random.randint(0, 200)) + random.randint(0, random.randint(0, 200))
#                           for i in range(20)]
#                          for j in range(5)]
#             group_labels = ['Radioplayer.ru', 'Телеканал КЛЮЧ', 'Дора',
#                             '4 Islands Swimrun St.Petersburg', 'Спорт ВКонтакте']
#             chart_data = pd.DataFrame(
#                 np.random.randn(30, 5),
#                 columns=group_labels)
#             st.line_chart(chart_data)
#             ###################################################################################################################
#             ###################################################################################################################
#
#             # exclude = st.text_input('Введите id групп, которые хотите исключить из графика')
#             ###################################################################################################################
#             ###################################################################################################################
#             comparison = st.text_input('Введите id групп, которые хотите сравнить')
#             if comparison:
#                 comparison = comparison.split(',')
#                 comparison = [i.strip() for i in comparison]
#                 hist_data = [np.random.randn(200) + np.random.randn(200) for i in comparison]
#                 fig = ff.create_distplot(hist_data, comparison)
#                 st.plotly_chart(fig, use_container_width=True)
#
#         case 'Анализ рекламы конкурентов':
#             competitor_advertising_analysis()
#
#         case "Аналитика активных клиентов":
#             st.markdown("Выберите конкурентов, по которым желаете фильтровать график")
#             sber = st.checkbox('СберБанк')
#             tink = st.checkbox('Тинькофф')
#             pochta = st.checkbox('ПочтаБанк')
#             alpha = st.checkbox('АльфаБанк')
#             vtb = st.checkbox('ВТБ')
#
#             if sber:
#                 st.markdown("СберБанк рекламируется в следующих группах:")
#                 x1 = np.random.randn(200) - 2
#                 x2 = np.random.randn(200)
#                 x3 = np.random.randn(200) + 2
#                 x4 = np.random.randn(200) + 3
#                 x5 = np.random.randn(200) + 4
#                 hist_data = [x1, x2, x3, x4, x5]
#                 group_labels = ['VK', 'Интернет сайты', 'YouTube', 'Ozon', 'Wildberries']
#                 fig = ff.create_distplot(
#                     hist_data, group_labels, bin_size=[.1, .25, .5, .10, .15])
#                 st.plotly_chart(fig, use_container_width=True)
#
#             if tink:
#                 st.markdown("Тинькофф рекламируется в следующих группах:")
#                 x1 = np.random.randn(200) - 2
#                 x2 = np.random.randn(200)
#                 x3 = np.random.randn(200) + 2
#                 x4 = np.random.randn(200) + 3
#                 x5 = np.random.randn(200) + 4
#                 hist_data = [x1, x2, x3, x4, x5]
#                 group_labels = ['VK', 'Интернет сайты', 'YouTube', 'Ozon', 'Wildberries']
#                 fig = ff.create_distplot(
#                     hist_data, group_labels, bin_size=[.1, .25, .5, .10, .15])
#                 st.plotly_chart(fig, use_container_width=True)
#
#             if pochta:
#                 st.markdown("ПочтаБанк рекламируется в следующих группах:")
#                 x1 = np.random.randn(200) - 2
#                 x2 = np.random.randn(200)
#                 x3 = np.random.randn(200) + 2
#                 x4 = np.random.randn(200) + 3
#                 x5 = np.random.randn(200) + 4
#                 hist_data = [x1, x2, x3, x4, x5]
#                 group_labels = ['VK', 'Интернет сайты', 'YouTube', 'Ozon', 'Wildberries']
#                 fig = ff.create_distplot(
#                     hist_data, group_labels, bin_size=[.1, .25, .5, .10, .15])
#                 st.plotly_chart(fig, use_container_width=True)
#
#             if alpha:
#                 st.markdown("АльфаБанк рекламируется в следующих группах:")
#                 x1 = np.random.randn(200) - 2
#                 x2 = np.random.randn(200)
#                 x3 = np.random.randn(200) + 2
#                 x4 = np.random.randn(200) + 3
#                 x5 = np.random.randn(200) + 4
#                 hist_data = [x1, x2, x3, x4, x5]
#                 group_labels = ['VK', 'Интернет сайты', 'YouTube', 'Ozon', 'Wildberries']
#                 fig = ff.create_distplot(
#                     hist_data, group_labels, bin_size=[.1, .25, .5, .10, .15])
#                 st.plotly_chart(fig, use_container_width=True)
#
#             if vtb:
#                 st.markdown("ВТБ рекламируется в следующих группах:")
#                 x1 = np.random.randn(200) - 2
#                 x2 = np.random.randn(200)
#                 x3 = np.random.randn(200) + 2
#                 x4 = np.random.randn(200) + 3
#                 x5 = np.random.randn(200) + 4
#                 hist_data = [x1, x2, x3, x4, x5]
#                 group_labels = ['VK', 'Интернет сайты', 'YouTube', 'Ozon', 'Wildberries']
#                 fig = ff.create_distplot(
#                     hist_data, group_labels, bin_size=[.1, .25, .5, .10, .15])
#                 st.plotly_chart(fig, use_container_width=True)
#
#         case "Пересечение аудитории с конкурентами":
#             options = st.multiselect(
#                 'Выберите конкурентов',
#                 ['СберБанк', 'Тинькофф', 'ПочтаБанк', 'АльфаБанк', 'ВТБ'])
#
#             hist_data = [
#                 random.randint(0, random.randint(0, 200)) + random.randint(0, random.randint(0, 200)) * 1000
#                 for i in options]
#             fig = px.pie(values=hist_data, names=options,
#                          title='Пересечение аудитории'
#                          )
#             st.plotly_chart(fig)
#
# #######################################################################################################################
# #######################################################################################################################
# case 'Интернет сайты':
#     match resp2 := sidebar2():
#         case 'Оценка площадки':
#             st.title(resp)
#             x1 = np.random.randn(200) - 2
#             x2 = np.random.randn(200)
#             x3 = np.random.randn(200) + 2
#             x4 = np.random.randn(200) + 3
#             x5 = np.random.randn(200) + 4
#
#             # Group data together
#             hist_data = [x1, x2, x3, x4, x5]
#
#             group_labels = ['VK', 'Интернет сайты', 'YouTube', 'Ozon', 'Wildberries']
#
#             # Create distplot with custom bin_size
#             fig = ff.create_distplot(
#                 hist_data, group_labels, bin_size=[.1, .25, .5, .10, .15])
#
#             # Plot!
#             st.plotly_chart(fig, use_container_width=True)
#             ###################################################################################################################
#             ###################################################################################################################
#
#             exclude = st.text_input('Введите id групп, которые хотите исключить из графика')
#             ###################################################################################################################
#             ###################################################################################################################
#             comparison = st.text_input('Введите название сайтов')
#             if comparison:
#                 comparison = comparison.split(',')
#                 comparison = [i.strip() for i in comparison]
#                 hist_data = [np.random.randn(200) + np.random.randn(200) for i in comparison]
#                 fig = ff.create_distplot(hist_data, comparison)
#                 st.plotly_chart(fig, use_container_width=True)
#
#         case 'Фильтр по конкурентам':
#             filter_competitors()
#
#         case "Оценка рекламного объявления":
#             object_competitors()
#
#         case 'Анализ рекламы конкурентов':
#             competitor_advertising_analysis()
#
#         case "Аналитика активных клиентов":
#             changing_audience()
#
#         case 'Оценка маржинальности рекламной кампании':
#             marginality()
#
#         case "Пересечение аудитории с конкурентами":
#             pass
#
# #######################################################################################################################
# #######################################################################################################################
# case 'YouTube':
#     match resp2 := sidebar2():
#         case 'Оценка площадки':
#             image = Image.open('images/youtube.jpeg')
#             st.image(image, width=100)
#             x1 = np.random.randn(200) - 2
#             x2 = np.random.randn(200)
#             x3 = np.random.randn(200) + 2
#             x4 = np.random.randn(200) + 3
#             x5 = np.random.randn(200) + 4
#
#             # Group data together
#             hist_data = [x1, x2, x3, x4, x5]
#
#             group_labels = ['Get Movies', 'Маша и Медведь', 'Mister Max', 'HiMan', 'Мирошка ТВ']
#
#             # Create distplot with custom bin_size
#             fig = ff.create_distplot(
#                 hist_data, group_labels, bin_size=[.1, .25, .5, .10, .15])
#             # exclude = st.text_input('Введите названия каналов, которые хотите исключить из графика')
#             # if exclude:
#             #     exclude = exclude.split(',')
#             #     exclude = [i.strip() for i in exclude]
#             #     result = list(set(group_labels) - set(exclude))
#             #     hist_data = [np.random.randn(200) + np.random.randn(200) for i in result]
#             #     fig = ff.create_distplot(hist_data, result)
#             #     st.plotly_chart(fig, use_container_width=True)
#             # else:
#             st.plotly_chart(fig, use_container_width=True)
#             ###################################################################################################################
#             ###################################################################################################################
#
#             ###################################################################################################################
#             ###################################################################################################################
#             comparison = st.text_input('Введите названия каналов, которые хотите сравнить')
#             if comparison:
#                 comparison = comparison.split(',')
#                 comparison = [i.strip() for i in comparison]
#                 hist_data = [np.random.randn(200) + np.random.randn(200) for i in comparison]
#                 fig = ff.create_distplot(hist_data, comparison)
#                 st.plotly_chart(fig, use_container_width=True)
#
#         case 'Фильтр по конкурентам':
#             filter_competitors()
#
#         case "Оценка рекламного объявления":
#             object_competitors()
#
#         case 'Анализ рекламы конкурентов':
#             competitor_advertising_analysis()
#
#         case "Аналитика активных клиентов":
#             changing_audience()
#
#         case 'Оценка маржинальности рекламной кампании':
#             marginality()
#
#         case "Пересечение аудитории с конкурентами":
#             pass
#
# #######################################################################################################################
# #######################################################################################################################
# case 'Ozon':
#     match resp2 := sidebar2():
#         case 'Оценка площадки':
#             image = Image.open('images/ozon.jpeg')
#             st.image(image)
#
#         case 'Фильтр по конкурентам':
#             filter_competitors()
#
#         case "Оценка рекламного объявления":
#             object_competitors()
#
#         case 'Анализ рекламы конкурентов':
#             competitor_advertising_analysis()
#
#         case "Аналитика активных клиентов":
#             changing_audience()
#
#         case 'Оценка маржинальности рекламной кампании':
#             marginality()
#
#         case "Пересечение аудитории с конкурентами":
#             pass
#
# #######################################################################################################################
# #######################################################################################################################
# case 'Wildberries':
#     match resp2 := sidebar2():
#         case 'Оценка площадки':
#             image = Image.open('images/wildberries.jpeg')
#             st.image(image)
#
#         case 'Фильтр по конкурентам':
#             filter_competitors()
#
#         case "Оценка рекламного объявления":
#             object_competitors()
#
#         case 'Анализ рекламы конкурентов':
#             competitor_advertising_analysis()
#
#         case "Аналитика активных клиентов":
#             changing_audience()
#
#         case 'Оценка маржинальности рекламной кампании':
#             marginality()
#
#         case "Пересечение аудитории с конкурентами":
#             pass
