import os
import pickle
from time import sleep

import requests

api_key = os.getenv('MEGAINDEX_KEY')
base_url = 'http://api.megaindex.com/{}'
all_domains = [
    'tinkoff.ru', 'sberbank.ru', 'uralsib.ru', 'vtb.ru', 'gazprombank.ru', 'alfabank.ru', 'rshb.ru', 'mkb.ru',
    'open.ru', 'sovcombank.ru', 'raiffeisen.ru', 'rosbank.ru', 'unicreditbank.ru', 'abr.ru', 'vbrr.ru', 'trust.ru',
    'domrfbank.ru',
    'bspb.ru', 'citibank.ru', 'smpbank.ru', 'novikom.ru', 'akbars.ru'
]
creds = {'host': os.getenv('POSTGRESQL_HOST'),
         'database': os.getenv('POSTGRESQL_DB'),
         'user': os.getenv('POSTGRESQL_USER'),
         'password': os.getenv('POSTGRESQL_PASS'),
         'port': os.getenv('POSTGRESQL_PORT')}


def req_to_megaindex(method, params):
    resp = requests.get(base_url.format(method), params=params)
    return resp.json()


# конкуренты
def seo_competitors():
    data = []
    for i, domain in enumerate(all_domains):
        print(i)
        resp = req_to_megaindex(
            f'visrep/competitors' + f'?key={api_key}&domain={domain}&search_engines[]=1&search_engines[]=2',
            params={})
        data_ = resp['data']
        result = []
        for el in data_:
            el.update(dict(main_bank=domain))
            el['competitor'] = el['domain']
            el.pop('domain')
            el.pop('site_id')
            result.append(el)
        data += result
        sleep(2)
    with open('competitors.pickle', 'wb') as f:
        pickle.dump(data, f)


enginies = {'status': 1, 'data': {'1': {'se_id': '1', 'name': 'Yandex', 'engine_type': 'SearchEngine'},
                                  '3': {'se_id': '3', 'name': 'Google', 'engine_type': 'SearchEngine'},
                                  '28': {'se_id': '28', 'name': 'Yandex Direct', 'engine_type': 'AdtSearchEngine'},
                                  '29': {'se_id': '29', 'name': 'Google Adwords', 'engine_type': 'AdtSearchEngine'}}, }


# по каким запросам ищут банк
def search_words():
    searchs = enginies['data']
    data = []
    for i, value in searchs.items():
        print(i, value)
        print(i)
        ser_id = value['se_id']
        source = value['name']
        resp = req_to_megaindex('visrep', dict(domain='uralsib.ru', ser_id=ser_id, count=100, key=api_key, desc=1))
        result = []
        for el in resp['data']:
            el.update(source=source)
            el.pop('path')
            el.pop('vis')
            el.pop('last_update')
            result.append(el)
        data += result
        sleep(2)

    with open('words_search.pickle', 'wb') as f:
        pickle.dump(data, f)
